/**
 * 
 */
package morris_java;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author speng created on Oct 6, 2015
 */
public class GameConsoleGui {
	private BufferedReader br = new BufferedReader(new InputStreamReader(
			System.in));

	public static void main(String[] args) {
		GameConsoleGui gameConsoleGui = new GameConsoleGui();
//		gameConsoleGui.run(false, PlayerType.HUMAN_VS_AI, false, 5);//humman vs AI
//		gameConsoleGui.run(false, PlayerType.HUMAN_VS_HUMAN, false, 5);//humman vs human
		gameConsoleGui.run(false, PlayerType.HUMAN_VS_HUMAN, false, 1);//
	}

	public void run(boolean loadGame, PlayerType playerType, boolean humanFirst, int level) {
		GameController game = null;
		if(loadGame) {
			game = GameController.loadGame();
		} else {
			game = new GameController();
			game.setAIlevel(level);
			game.setPlayerType(playerType);
		}
		GameLog gameLog = new GameLog();
		Player AI_player = game.getPlayer_1();
		AI ai = new AI(game.getAIlevel(), AI_player);
		AI test_ai = new AI(-1, game.getPlayer_2());
		gameLog.addToLog("playerType = " + game.getPlayerType().toString());
		gameLog.addToLog("difficultyLevel = " + game.getAIlevel());
		while (!game.getState().equals(GameState.PLAYER_1_WIN) 
				&& !game.getState().equals(GameState.PLAYER_2_WIN) ) {
			gameLog.addToLog(game.getBoardLayout());
			gameLog.addToLog(game.getCurrentPlayer().toString());
			gameLog.addToLog(game.getState().toString());
			if(game.getState().equals(GameState.MOVE_2) || game.getState().equals(GameState.FLY_2)) {
				gameLog.addToLog(game.getPotentialPos().toString());
			}
			Coord coord = null;		
			switch (game.getPlayerType()) {
			case HUMAN_VS_HUMAN:
				coord = getInputCoord();
				break;
			case HUMAN_VS_AI:
				if(game.getCurrentPlayer().equals(AI_player)) {
					coord = ai.makeMove(game);
				} else {
					coord = getInputCoord();
				}
				break;
			case AI_VS_AI:
				if(game.getCurrentPlayer().equals(AI_player)) {
					coord = ai.makeMove(game);
				} else {
					coord = test_ai.makeMove(game);
				}
				break;
			default:
				System.err.println("invalid player type");
				break;
			}
			
			gameLog.addToLog(coord.toStringDebug());
			Position pos = new Position(coord, game.getCurrentPlayer());
			if (!game.move(pos)) {
				gameLog.addToLog("illegal move");
			}
			GameController.saveGame(game);
		}
		gameLog.addToLog(game.getBoardLayout());
		gameLog.addToLog(game.getState().toString());
		gameLog.addToLog("game finished in " + game.getNumMoves() + " moves");
		gameLog.close();
	}

	private Coord getInputCoord() {
		String userInput = null;

		try {
			userInput = br.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return parseInput(userInput);
	}

	private Coord parseInput(String userInput) {
		Pattern p = Pattern
				.compile("\\s*\\(\\s*(\\d)\\s*,\\s*(\\d)\\s*\\)\\s*");
		Matcher m = p.matcher(userInput);
		if (m.matches()) {
			int x = Integer.parseInt(m.group(1));
			int y = Integer.parseInt(m.group(2));
			Coord coord = new Coord(x, y);
			return coord;
		}
		return null;
	}

}


