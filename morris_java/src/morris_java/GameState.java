package morris_java;
enum GameState {
	PLAYER_1_WIN,
	PLAYER_2_WIN,
	ILLEGAL_MOVE,
	DROP,
	MOVE_1,
	MOVE_2,
	FLY_1,
	FLY_2,
	NEW_MILL
}
